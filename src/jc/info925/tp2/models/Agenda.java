/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO 925 TP2
 */
package jc.info925.tp2.models;

import jc.info925.tp2.utils.Constant;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Agenda {

    private Map<String, Meeting> meetings;

    public Agenda() {

        this.meetings = new ConcurrentHashMap<String, Meeting>();
        Long currentTimestamp = (new Date()).getTime();
        int number = 0;
        for (long start = currentTimestamp; start < currentTimestamp + Constant.AMPLITUDE; start+=2*Constant.HOUR) {
            long duration = (long) Math.floor(Math.random()*3*Constant.HOUR);
            if (Math.random() > Constant.PERSONAL_OCCUPATION && this.isFree(start, start + duration)) {
                this.meetings.put("Busy    " + number, new Meeting(start, start + duration));
                number++;
            }
        }
    }

    public void add(String name, Meeting meeting) {
        if(this.isFree(meeting.start,meeting.end)) {
            this.meetings.put(name, meeting);
        }
    }

    public void completeDoodle(Doodle doodle, long id) {
        for(Long timestamp : doodle.timeslots.keySet()) {
            if (this.isFree(timestamp, timestamp + doodle.duration)) {
                doodle.timeslots.get(timestamp).add(id);
            }
        }
    }

    public void display(Long id) {

        Map<String, Meeting> meetingMap = this.sortByComparator(this.meetings);
        String agenda = "Agenda de l'agent " + id + "\n";

        for (String name : meetingMap.keySet()) {
            Meeting meeting = meetingMap.get(name);
            Date start = new Date(meeting.start);
            Date end = new Date(meeting.end);
            agenda += start.toString() + " -- " + name + " -- " + end.toString() + "\n";
        }

        System.out.println(agenda);
    }

    private boolean isFree(long start, long end) {
        Date debut = new Date(start);
        Date fin = new Date(end);
        if (debut.getHours() < 7 || fin.getHours() > 19 || debut.getHours() > 18) {
            return false;
        }
        for(String name : this.meetings.keySet()) {
            Meeting curentMeeting = this.meetings.get(name);
            if ((end < curentMeeting.end && end > curentMeeting.start)
                    || (start > curentMeeting.start && start < curentMeeting.end)) {
                return false;
            }
        }
        return true;
    }

    public void addForce(String action, Meeting meeting) {

        for(String name : this.meetings.keySet()) {
            Meeting curentMeeting = this.meetings.get(name);
            if ((meeting.end < curentMeeting.end && meeting.end > curentMeeting.start)
                    || (meeting.start > curentMeeting.start && meeting.start < curentMeeting.end)) {
                this.meetings.remove(name);
            }
        }
        this.add(action, meeting);
    }

    private Map<String, Meeting> sortByComparator(Map<String, Meeting> unsortMap) {

        // Convert Map to List
        List<Map.Entry<String, Meeting>> list =
                new LinkedList<Map.Entry<String, Meeting>>(unsortMap.entrySet());

        // Sort list with comparator, to compare the Map values
        Collections.sort(list, new Comparator<Map.Entry<String, Meeting>>() {
            public int compare(Map.Entry<String, Meeting> o1,
                               Map.Entry<String, Meeting> o2) {
                return (o1.getValue().start) >= (o2.getValue().start) ? (o1.getValue().start) == (o2.getValue().start) ? 0 : 1 : -1;
            }
        });

        // Convert sorted map back to a Map
        Map<String, Meeting> sortedMap = new LinkedHashMap<String, Meeting>();
        for (Iterator<Map.Entry<String, Meeting>> it = list.iterator(); it.hasNext();) {
            Map.Entry<String, Meeting> entry = it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }
}

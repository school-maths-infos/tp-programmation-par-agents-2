/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO 925 TP2
 */
package jc.info925.tp2.models;

public class Meeting {

    public long start;
    public long end;

    public Meeting(long start, long end) {
        this.start = start;
        this.end = end;
    }
}

/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO 925 TP2
 */

package jc.info925.tp2.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Doodle implements Serializable {

    public int duration;
    public Map<Long, ArrayList<Long>> timeslots;
    public int nbPersonMin;

    public Doodle(String serialized) {

        String[] values = serialized.split("<");

        this.duration = Integer.parseInt(values[0].split(",")[0]);
        this.nbPersonMin = Integer.parseInt(values[0].split(",")[1]);
        this.timeslots = new HashMap<Long, ArrayList<Long>>();

        for (int i = 1; i < values.length; i++) {
            String[] timestampValues = values[i].split("-");
            ArrayList<Long> ids = new ArrayList<Long>();
            for(int j = 1; j < timestampValues.length; j++) {
                ids.add(Long.parseLong(timestampValues[j]));
            }
            this.timeslots.put(Long.parseLong(timestampValues[0]), ids);
        }
    }

    public Doodle(int duration, Date start, Date end, int interval, int nbPersonMin) {
        this.duration = duration;
        this.timeslots = new HashMap<Long, ArrayList<Long>>();
        this.initializeTimeslot(start, end, interval);
        this.nbPersonMin = nbPersonMin;
    }

    private void initializeTimeslot(Date start, Date end, int interval) {
        long timestamptStart = start.getTime();
        long timestamptEnd = end.getTime();

        for (long i = timestamptStart; i < timestamptEnd; i += interval) {
            this.timeslots.put(i, new ArrayList<Long>());
        }
    }

    public String toString() {
        String s = duration + "," + nbPersonMin + "";
        for(Long timestamp: this.timeslots.keySet()) {
            s += "<" + timestamp;
            for(Long id : this.timeslots.get(timestamp)) {
                s += "-" + id;
            }
        }
        return s;
    }

    public void fill(String serialized) {

        String[] values = serialized.split("<");

        for (int i = 1; i < values.length; i++) {
            String[] timestampValues = values[i].split("-");
            for(int j = 1; j < timestampValues.length; j++) {
                this.timeslots.get(Long.parseLong(timestampValues[0])).add(Long.parseLong(timestampValues[j]));
            }
        }

    }
}

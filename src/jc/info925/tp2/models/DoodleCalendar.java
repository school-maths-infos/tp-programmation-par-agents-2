/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO 925 TP2
 */
package jc.info925.tp2.models;

import java.util.HashMap;

public class DoodleCalendar {

    private static DoodleCalendar instance;
    public HashMap<String,Doodle> availableDoodles = new HashMap<String, Doodle>();

    public static DoodleCalendar getInstance() {
        if (instance == null) {
            instance = new DoodleCalendar();
        }
        return instance;
    }
}

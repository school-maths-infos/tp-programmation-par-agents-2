/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO 925 TP2
 */
package jc.info925.tp2.multiagent.core;
import jc.info925.tp2.multiagent.example.MAS;

import com.rabbitmq.client.*;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public abstract class Receiver {

    protected Long id;
    protected Agent parent;
    protected Channel channel;
    protected String queueName;
    protected Connection connection;
    protected Consumer consumer;

    public Receiver(Agent parent, Long id) throws Exception {

        this.id = id;
        this.parent = parent;

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        connection = factory.newConnection();
        channel = connection.createChannel();

        channel.exchangeDeclare(MAS.EXCHANGE_NAME, "fanout");
        queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, MAS.EXCHANGE_NAME, "");

        consumer = new QueueingConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
                                       byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                handleCommonMessages(message);
                handleMessages(message);
            }

        };
        channel.basicConsume(queueName, true, consumer);

    }

    protected void handleCommonMessages(String message) {

        /** MESSAGE D'ENTREE DANS LE GROUPE **/
        if (message.equals("Welcome to group")) {
            this.parent.broadcast(MAS.EXCHANGE_NAME, " [" + this.id + "] enters the group");
        }
        else if (message.equals("stop")) {
            System.err.println(" [" + this.id + "] Received Stop Signal");
            try {
                consumer.handleCancel("stop");
                channel.close();
                connection.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }
        }
    }

    protected abstract void handleMessages(String message);
}

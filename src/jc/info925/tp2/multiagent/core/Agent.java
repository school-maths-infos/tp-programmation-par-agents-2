/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO 925 TP2
 */
package jc.info925.tp2.multiagent.core;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public abstract class Agent {

	protected long id;
	private String mailbox;
	private Channel channel;

	public Agent(long id, String name) {

		this.id = id;
		this.mailbox = name;
		init();
	}

	private void init() {

        ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		Connection connection;
		try {
			connection = factory.newConnection();
			channel = connection.createChannel();
			channel.queueDeclare(mailbox, false, false, false, null);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
	}

	public void broadcast(String exchange, String message) {
		try {
			channel.exchangeDeclare(exchange, "fanout");
			String queueName = channel.queueDeclare().getQueue();
			channel.queueBind(queueName, exchange, "");

		    channel.basicPublish(exchange, "", null, message.getBytes("UTF-8"));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

}

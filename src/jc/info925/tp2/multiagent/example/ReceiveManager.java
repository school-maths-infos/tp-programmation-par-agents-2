/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO 925 TP2
 */
package jc.info925.tp2.multiagent.example;

import jc.info925.tp2.models.Doodle;
import jc.info925.tp2.models.DoodleCalendar;
import jc.info925.tp2.multiagent.core.Agent;
import jc.info925.tp2.multiagent.core.Receiver;
import jc.info925.tp2.utils.Constant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Stack;

public class ReceiveManager extends Receiver implements Runnable {

    protected int nbResponseMin = Constant.MAX_PERSON - 1;
    protected int nbAckMin = Constant.MAX_PERSON - 1;
    protected int nbACK;
    protected int nbResponse;
    protected Stack<String> meetingsProposal;
    protected ArrayList<Long> agents;

    public ReceiveManager(Agent parent, Long id) throws Exception {
        super(parent,id);
        this.meetingsProposal = new Stack<String>();
	}

	protected void handleMessages(String message) {

        if (message.equals("START 12")) {
            for (int i = 0; i < 3; i++) {
                this.meetingsProposal.push("Meeting " + i);
            }
            this.sendDoodleProposal();
        }
        /** UN AGENT ME DONNE SES DATES DISPOS POUR LA REUNION QUE J'AI ENVOYE EN new_doodle **/
        else if (message.split(":")[0].equals("propose")) {
            System.out.println(" [" + this.id + "] Received doodle proposal for "
                    + message.split(":")[1] + " by Agent " + message.split(":")[3] );
            try {
                Thread.sleep(Constant.sleep_time());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.receiveDoodleResponse(message);
		}
        /** L'AGENT M'AVERTIT QU'IL A BIEN AJOUTE LA REUNION DANS SON AGENDA **/
        else if (message.split(":")[0].equals("ack")) {
            System.out.println(" [" + this.id + "] Received the meeting is added in agent's agenda.");
            try {
                Thread.sleep(Constant.sleep_time());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.receiveACK();
        }
	}

    private void receiveACK() {

        this.nbACK++;
        if (this.nbACK >= this.nbAckMin) {
            this.sendDoodleProposal();
        }
    }

    /** Un agent a envoyé ses dates, on les enregistre et si tous les agents ont répondu on fixe la réunion.
     * On prévient les agents **/
    private void receiveDoodleResponse(String message) {

        String action = message.split(":")[1];
        Doodle doodle = DoodleCalendar.getInstance().availableDoodles.get(action);
        String mondoodle = message.split(":")[2];
        doodle.fill(mondoodle);
        this.nbResponse++;
        agents.add(Long.parseLong(message.split(":")[3]));
        if (this.nbResponse == this.nbResponseMin) {
            organizeMeeting(action, doodle);
        }
    }

    private void organizeMeeting(String action, Doodle doodle) {

        int max = 0;
        long timeslotAccepted = 0;
        for(Long timestamp : doodle.timeslots.keySet()) {
            int nbAccepted = doodle.timeslots.get(timestamp).size();
            if (nbAccepted > max) {
                max = nbAccepted;
                timeslotAccepted = timestamp;
            }
        }
        ArrayList<Long> listAgentsAccept;
        if(timeslotAccepted != 0) {
            listAgentsAccept = doodle.timeslots.get(timeslotAccepted);
        } else {
            listAgentsAccept = new ArrayList<Long>();
        }
        //Voir si max >= nbPersonneMinimal
        if (max >= doodle.nbPersonMin) {
            sendAcceptToAllAvailableAgents(action, doodle, timeslotAccepted, listAgentsAccept);
        }
        //Faire le cas force_timeslot en disant aux gens de s'arranger
        else {
            sendAcceptToAllAvailableAgents(action, doodle, timeslotAccepted, listAgentsAccept);
            sendRejectToAllForcedAgents(action, doodle, max, timeslotAccepted, listAgentsAccept);
        }
    }

    private void sendRejectToAllForcedAgents(String action, Doodle doodle, int max, long timeslotAccepted, ArrayList<Long> listAgentsAccept) {
        int nbAgents = doodle.nbPersonMin - max;
        ArrayList<Long> listAgentsRefuse = new ArrayList<Long>();
        for(Long agentId : agents) {
            if (!listAgentsAccept.contains(agentId)) {
                listAgentsRefuse.add(agentId);
            }
        }
        ArrayList<Long> listAgentsForced = new ArrayList<Long>();
        for (int i = 0; i < nbAgents; i++) {
            int random = (int) Math.floor(Math.random() * (listAgentsRefuse.size() - 1));
            Long idAgentForced = listAgentsRefuse.remove(random);
            listAgentsForced.add(idAgentForced);
        }

        if (listAgentsForced.size() > 0) {
            String agentsForced = "";
            for (Long id : listAgentsForced) {
                agentsForced += id + ",";
            }

            this.nbAckMin += listAgentsForced.size();

            this.parent.broadcast(
                    MAS.EXCHANGE_NAME,
                    "force_timeslot:" + action
                            + ":" + timeslotAccepted
                            + ":" + doodle.duration
                            + ":" + agentsForced.substring(0, agentsForced.length() - 1));
        }
    }

    private void sendAcceptToAllAvailableAgents(String action, Doodle doodle, long timeslotAccepted, ArrayList<Long> listAgentsAccept) {
        if(listAgentsAccept.size() > 0) {
            String agentsStr = "";
            for (Long id : listAgentsAccept) {
                agentsStr += id + ",";
            }
            this.nbAckMin = listAgentsAccept.size();
            this.parent.broadcast(
                    MAS.EXCHANGE_NAME,
                    "accept_timeslot:" + action
                            + ":" + timeslotAccepted
                            + ":" + doodle.duration
                            + ":" + agentsStr.substring(0, agentsStr.length() - 1)
            );
        }
    }

    /** Démarrer la prochaine réunion de la pile **/
    private void sendDoodleProposal() {

        try {
            channel.queuePurge(queueName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.nbResponse = 0;
        this.nbACK = 0;
        this.agents = new ArrayList<Long>();

        if (this.meetingsProposal.size() > 0) {
            String key = this.meetingsProposal.pop();
            Date now = new Date();
            Date begin = new Date(now.getTime() + (long) Math.random() * Constant.AMPLITUDE + Constant.HOUR);
            Doodle doodle = new Doodle(
                    4 * Constant.HOUR,
                    begin,
                    new Date(begin.getTime() + Constant.AMPLITUDE),
                    4 * Constant.HOUR,
                    (int) Math.floor(Math.random() * (Constant.MAX_PERSON - Constant.MIN_PERSON) + Constant.MIN_PERSON)
            );
            DoodleCalendar.getInstance().availableDoodles.put(key, doodle);

            this.parent.broadcast(MAS.EXCHANGE_NAME, "new_doodle:" + key + ":" + doodle);
        } else {
            this.parent.broadcast(MAS.EXCHANGE_NAME, "stop");
        }
    }


    public void run() {

    }
}

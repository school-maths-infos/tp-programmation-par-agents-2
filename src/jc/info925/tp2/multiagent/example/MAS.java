/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO 925 TP2
 */
package jc.info925.tp2.multiagent.example;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import jc.info925.tp2.utils.Constant;

public class MAS {

	public static final String EXCHANGE_NAME = "group";

	public MAS() {

		try {
			Thread.sleep(3* Constant.SECOND);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		Connection connection;
		try {
			connection = factory.newConnection();
			Channel channel = connection.createChannel();

			channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
		    String message = "Welcome to group";

		    channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes("UTF-8"));
		    System.out.println(" [MAS] Sent '" + message + "'");

		    message = "START 12";

		    channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes("UTF-8"));
		    System.out.println(" [MAS] Sent '" + message + "'");

		} 
		catch (IOException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new MAS();
	}
}

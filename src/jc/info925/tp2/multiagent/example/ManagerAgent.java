/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO 925 TP2
 */
package jc.info925.tp2.multiagent.example;

import jc.info925.tp2.multiagent.core.Agent;

public class ManagerAgent extends Agent {

    public ManagerAgent(long id, String name) {
        super(id, name);
        try {
            new Thread(new ReceiveManager(this, id));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new ManagerAgent(new Long(args[0]), args[1]);
    }
}

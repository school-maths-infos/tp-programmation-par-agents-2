/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO 925 TP2
 */
package jc.info925.tp2.multiagent.example;

import jc.info925.tp2.multiagent.core.Agent;

public class SimpleAgent extends Agent {

	public SimpleAgent(long id, String name) {
		super(id, name);
		try {
			new Thread(new ReceiveAgent(this, id));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new SimpleAgent(new Long(args[0]), args[1]);
	}
}

/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO 925 TP2
 */
package jc.info925.tp2.multiagent.example;

import jc.info925.tp2.models.Agenda;
import jc.info925.tp2.models.Doodle;
import jc.info925.tp2.models.Meeting;
import jc.info925.tp2.multiagent.core.Agent;
import jc.info925.tp2.multiagent.core.Receiver;
import jc.info925.tp2.utils.Constant;

public class ReceiveAgent extends Receiver implements Runnable {

    protected Agenda agenda;

    public ReceiveAgent(Agent parent, Long id) throws Exception {
        super(parent,id);
        this.agenda = new Agenda();
	}

	protected void handleMessages(String message) {

        /** LE MANAGER TE PROPOSE UNE REUNION **/
        if (message.split(":")[0].equals("new_doodle")) {
            System.out.println(" [" + this.id + "] Received doodle request for " + message.split(":")[1]);
            try {
                Thread.sleep(Constant.sleep_time());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.respondNewDoodle(message);
		}
        /** LE MANAGER A BIEN RECU TES DATES DISPOS ET A DECIDE LA DATE FINALE DE LA REUNION **/
        else if (message.split(":")[0].equals("accept_timeslot")) {

            String strIds = message.split(":")[4];
            String ids[] = strIds.split(",");
            for (String id : ids) {
                if (Long.parseLong(id) == this.id) {
                    System.out.println(" [" + this.id + "] Received the validate meeting scheduled.");
                    try {
                        Thread.sleep(Constant.sleep_time());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    this.receiveAcceptTimeslot(message);
                    break;
                }
            }
		}
        /** TOUT LE MONDE N'EST PAS DISPO EN MEME TEMPS, IL FAUT SE DEBROUILLER **/
        //Si nombre de personne >= nbMinPourLaReunion alors on l'organise quand même (cas OUI)
        //Sinon on récupère les personnes les plus importantes (pour compléter ce qui manque jusqu'au nb minimum)
        // et on leur impose de s'arranger. (cas NON) . Qui est important = personnes tirées au hasard pour la simul
        /** On a reçu force_timeslot, qui nous impose de nous arranger **/
        else if (message.split(":")[0].equals("force_timeslot")) {

            String strIds = message.split(":")[4];
            String ids[] = strIds.split(",");
            for (String id : ids) {
                if (Long.parseLong(id) == this.id) {
                    System.out.println(" [" + this.id + "] Received the forced proposal.");
                    try {
                        Thread.sleep(Constant.sleep_time());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    this.receiveForceTimeslot(message);
                    break;
                }
            }
        }

	}

    private void receiveForceTimeslot(String message) {

        String[] values = message.split(":");
        String action = values[1];
        long start = Long.parseLong(values[2]);
        long duration = Long.parseLong(values[3]);
        long end = start + duration;
        Meeting meeting = new Meeting(start, end);
        this.agenda.addForce(action, meeting);
        this.parent.broadcast(MAS.EXCHANGE_NAME, "ack:" + action);
    }

    /** La date de réunion a été définie => il faut l'enregistrer dans mon agenda et envoyer un ACK au manage **/
    private void receiveAcceptTimeslot(String message) {

        String[] values = message.split(":");
        String action = values[1];
        long start = Long.parseLong(values[2]);
        long duration = Long.parseLong(values[3]);
        long end = start + duration;
        Meeting meeting = new Meeting(start, end);
        this.agenda.add(action, meeting);
        this.agenda.display(this.id);
        this.parent.broadcast(MAS.EXCHANGE_NAME, "ack:" + action);
    }

    /** Le manager m'a envoyé une proposition de Doodle -> je lui donne mes dispos **/
    private void respondNewDoodle(String message) {

        String action = message.split(":")[1];
        String mondoodle = message.split(":")[2];
        Doodle doodle = new Doodle(mondoodle);
        this.agenda.completeDoodle(doodle, this.id);
        this.parent.broadcast(MAS.EXCHANGE_NAME, "propose:" + action + ":" + doodle + ":" + this.id);
    }

    public void run() {
    }
}

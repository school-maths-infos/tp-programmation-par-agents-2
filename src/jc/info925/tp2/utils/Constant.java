/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO 925 TP2
 */
package jc.info925.tp2.utils;

public class Constant {

    public static final double PERSONAL_OCCUPATION = 0.05;
    public static int MIN_PERSON = 4;
    public static int MAX_PERSON = 5;
    public static final int SECOND = 1000; //milliseconds
    public static final int HOUR = 3600* SECOND;
    public static final int DAY = 24* HOUR;
    public static final int AMPLITUDE = 3* DAY;

    public static int sleep_time() {
        return (int) (Math.random()*SECOND);
    }
}

#!/bin/bash

process_id=`ps aux | grep "java -cp .:amqp-client-3.6.0.jar jc.info925.tp2.multiagent.example" | grep -v grep | awk '{print $2}'`
if [ $? -eq "0" ]; then
kill -9 $process_id
fi
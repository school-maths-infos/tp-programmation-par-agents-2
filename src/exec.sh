#!/bin/bash

clear
echo ""
echo "Compilation en cours ..."

javac -Xlint:deprecation jc/info925/tp2/models/*.java
javac -cp ".:amqp-client-3.6.0.jar" -Xlint:deprecation jc/info925/tp2/multiagent/core/Agent.java
javac -cp ".:amqp-client-3.6.0.jar" -Xlint:deprecation jc/info925/tp2/multiagent/example/*.java

echo "Compilation terminée, patientez 10s pour le démarrage"

java -cp ".:amqp-client-3.6.0.jar" jc.info925.tp2.multiagent.example.ManagerAgent 12 toto &
java -cp ".:amqp-client-3.6.0.jar" jc.info925.tp2.multiagent.example.SimpleAgent 1 toto &
java -cp ".:amqp-client-3.6.0.jar" jc.info925.tp2.multiagent.example.SimpleAgent 2 toto &
java -cp ".:amqp-client-3.6.0.jar" jc.info925.tp2.multiagent.example.SimpleAgent 3 toto &
java -cp ".:amqp-client-3.6.0.jar" jc.info925.tp2.multiagent.example.SimpleAgent 4 toto &
java -cp ".:amqp-client-3.6.0.jar" jc.info925.tp2.multiagent.example.SimpleAgent 5 toto &
java -cp ".:amqp-client-3.6.0.jar" jc.info925.tp2.multiagent.example.MAS

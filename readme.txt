/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO 925 TP2
 */

//////////////////////////////////
// Compilation et exécution :   //
//////////////////////////////////

Placez vous dans le dossier src.

./exec.sh     : Pour compiler et exécuter le programme (l'ensemble des agents + le MAS)
./verif.sh    : Pour vérifier quels sont les agents en cours d'exécution
./stop.sh     : Pour arrêter l'exécution (tous les agents et le MAS sont tués)

//////////////////////////////////
// Protocole de communication : //
//////////////////////////////////

Agent                Manager
  |                     |
  |   new_doodle        |
  | < ----------------  | Le manager envoie une offre de Doodle (différents créneaux + durée prévue)
  |                     |
  |      propose        |
  | ----------------- > | L'agent indique les créneaux disponibles parmis ceux proposés
  |                     |
  |                     |
  |                     | Après avoir reçu toutes les réponses des agents, le Manager cherche un créneaux ou le nombre
  |       accept        | minimal de personnes est atteint.
  |  < ---------------- | Il envoie le créneaux choisi à tous ceux qui étaient dispos.
  |      ou force       |
  |  < ---------------- | Si il manque des personnes, il choisit des personnes à qui il va imposer le créneaux choisi.
  |                     |  Les personnes sont alors obligées d'annuler leurs autres rdv pour se rendre dispo.
  |                     |
  |                     |
  |                     | L'agent ajoute la réunion dans son agenda (si il a eu accept c'est facile, si il a eu force,
  |      ack            |   il annule ses autres rdv).
  | ----------------- > | Lorsque c'est fait il confirme au manager que c'est fait
  |                     |
  |                     | Le manager attend d'avoir reçu tous les ACK,
  |      new_doodle            |
  | < ----------------  | puis il passe au Doodle suivant
  |                     |
  |                     |

//////////////////////////////////
// Visualiser l'exécution :     //
//////////////////////////////////

Les échanges montrés à la section "Protocole de communication" sont visibles dans la console d'exécution.
Le manager est l'agent numéro 12. Les autres sont des agents standards.
Nous avons ajouté des "Thread.sleep" aléatoires à chaque étape sur les agents, afin que l'humain
lise plus facilement la console.
Au début de l'exécution, nous faisons patienter l'utilisateur 10s.
En effet, il faut être sûrs que tous les agents aient commencé d'écouter avant que le MAS envoie le message de
bienvenue.

Echange :
    new_doodle : Le manager envoie une offre de Doodle (différents créneaux + durée prévue)
A voir dans la console :
    Chaque agent reçoit le new_doodle :
        [5] Received doodle request for Réunion 0

Echange :
    propose : L'agent indique les créneaux disponibles parmis ceux proposés
A voir dans la console :
    Le manager reçoit les propositions de chaque agent :
        [12] Received doodle proposal for Réunion 2 by Agent 2

Echange :
    accept : Le manager envoie le créneaux choisi à tous ceux qui étaient dispos.
    force : Le manager choisit des personnes à qui il va imposer le créneaux choisi
A voir dans la console :
    Chaque agent disponible reçoit le créneau en mode "accept" :
        [16] Received the validate meeting scheduled.
    Chaque agent forcé reçoit le créneau en mode "force" :
        [5] Received the forced proposal.
    Les agents affichent l'état courant de leur agenda pour que le programmeur voit que ça a fonctionné correctement.
        Agenda de l'agent 14
        Sat Mar 12 07:03:28 CET 2016 -- Meeting 0 -- Sat Mar 12 11:03:28 CET 2016
        Sat Mar 12 11:03:28 CET 2016 -- Meeting 1 -- Sat Mar 12 15:03:28 CET 2016
        Sun Mar 13 23:03:27 CET 2016 -- Meeting 2 -- Mon Mar 14 03:03:27 CET 2016
        Fri Mar 18 12:03:18 CET 2016 -- Busy    1 -- Fri Mar 18 14:55:41 CET 2016
            (  Meeting [num] signifie : une réunion organisée via doodle  )
            (  Busy    [num] signifie : une activité prévue au départ dans l'agenda de la personne  )

Echange :
    ack : L'agent indique au manager qu'il a bien ajouté la réunion dans son agenda.
A voir dans la console :
    Le manager reçoit les ack de chaque agent :
        [12] Received the meeting is added in agent's agenda.
    On peut voir que le manager a bien reçu tous les ack car il passe au new_doodle suivant OU il envoie le signal "stop"
        [5] Received Stop Signal
Remarque : notre stop ne fonctionne pas, il faut quand même utiliser le script stop.sh pour arrêter les processus.